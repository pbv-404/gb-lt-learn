<?php

namespace common\models;

use function array_keys;
use common\services\tasks\MessengersTypeEnum;
use DateTime;
use function is_numeric;
use function max;
use yii\base\Model;

class TaskForm extends Model
{
    const MESSENGER_TYPES = [
        MessengersTypeEnum::TELEGRAM => 'Telegram',
        MessengersTypeEnum::VIBER => 'Viber',
        MessengersTypeEnum::WHATSAPP => 'Whatsapp',
        MessengersTypeEnum::ALL => 'All',
    ];

    /** @var string */
    public $sendTo;
    /** @var string */
    public $message;
    /** @var int */
    public $messengerType;
    /** @var DateTime */
    public $dateSend;

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['sendTo', 'message', 'messengerType', 'dateSend'], 'required'],
            ['messengerType', 'validateMessengerType'],
            ['dateSend', 'validateDate'],
        ];
    }

    /**
     * Выполняет проверку что тип мессенжера находится в допустимом диапазоне.
     */
    public function validateMessengerType(string $attribute, ?array $params): bool
    {
        if (false === is_numeric($this->messengerType)) {
            $this->addError($attribute, 'Incorrect messengerType!');

            return false;
        }
        $value = (int) ($this->messengerType);
        if ($value <= 0 || $value > max(array_keys(self::MESSENGER_TYPES))) {
            $this->addError($attribute, 'messengerType out of range!');

            return false;
        }
        $this->messengerType = $value;

        return true;
    }

    /**
     * Правила валидации даты.
     *
     * @param string     $attribute the attribute currently being validated
     * @param array|null $params    the additional name-value pairs given in the rule
     */
    public function validateDate(string $attribute, ?array $params): bool
    {
        if ($this->dateSend instanceof DateTime) {
            $isDate = true;
        } else {
            $isDate = false !== strtotime($this->dateSend);
        }
        if (false === $isDate) {
            $this->addError($attribute, 'Incorrect date to send!');

            return $isDate;
        }
        $date = new DateTime($this->dateSend, new \DateTimeZone('+0300'));
        if ($date->getTimestamp() < (new DateTime('now', new \DateTimeZone('+0300')))->getTimestamp()) {
            $this->addError($attribute, 'Can not send a message to the past!');

            return false;
        }
        $this->dateSend = $date;

        return true;
    }
}
