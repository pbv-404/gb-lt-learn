<?php
declare(strict_types=1);

namespace common\services\tasks\contract;

use yii\queue\cli\Queue;
use yii\queue\JobInterface;

/**
 * Interface MessengerSenderContract.
 *
 * Связующее звено между структурой
 * и выполнением отправки в конкретный мессенджер
 */
interface MessengerSenderContract extends JobInterface, MessengerStructureContract
{
    /**
     * точка входа для выполнения инструкций подписчика.
     */
    public function update(MessageSubscriberContract $subject, MessengerStructureContract $data, ?Queue $queue = null): string;
}
