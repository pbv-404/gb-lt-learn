<?php
declare(strict_types=1);

namespace common\services\tasks\contract;

use DateTime;

/**
 * Interface MessengerStructureContract.
 *
 * Структура передаваемая в мессенджеры
 */
interface MessengerStructureContract
{
    /**
     * Загружает переданную структуру в структуру класса.
     */
    public function loadStructure(self $structure): self;

    /**
     * Возвражает наименование мессенджера.
     */
    public function getMessengerName(): string;

    /**
     * Устанавливает наименование мессенджера.
     */
    public function setMessengerName(string $messengerName): self;

    /**
     * Возвращает ключ отправителя.
     */
    public function getSendTo(): string;

    /**
     * Устанавливает ключ отправителя.
     */
    public function setSendTo(string $sendTo): self;

    /**
     * Возвращает тело сообщения.
     */
    public function getMessage(): string;

    /**
     * Устанавливает тело сообщения.
     */
    public function setMessage(string $message): self;

    /**
     * Возвращает дату, когда должно быть отправлено сообщение.
     */
    public function getNeedSend(): DateTime;

    /**
     * Устанавливает дату, когда должно быть отправлено сообщение.
     */
    public function setNeedSend(DateTime $needSend): self;
}
