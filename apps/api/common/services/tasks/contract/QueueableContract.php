<?php
declare(strict_types=1);

namespace common\services\tasks\contract;

use yii\queue\cli\Queue;

interface QueueableContract
{
    /**
     * Устанавливает обработчик очередей, для пушинга в демона.
     */
    public function setQueue(Queue $queue): void;

    /**
     * Возвращает обработчик очередей.
     */
    public function getQueue(): ?Queue;
}
