<?php
declare(strict_types=1);

namespace common\services\tasks\contract;

/**
 * Interface MessageSubscriberContract.
 *
 * Контракт для работы с подписками на отправку сообщений
 */
interface MessageSubscriberContract
{
    /**
     * Добавляет обработчик событий к наблюдателям
     */
    public function attach(MessengerSenderContract $observer, int $messengerType = 1): self;

    /**
     * Удаляет обработчик событий из наблюдателя.
     */
    public function detach(MessengerSenderContract $observer, int $messengerType = 1): self;

    /**
     * Выполняет зарегистрированные в наблюдатели подпски.
     *
     * @return MessengerSenderContract[]
     */
    public function notify(int $messengerType = 0, MessengerStructureContract $data): array;
}
