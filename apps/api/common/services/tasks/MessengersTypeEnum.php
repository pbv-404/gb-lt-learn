<?php
declare(strict_types=1);

namespace common\services\tasks;

/**
 * Class MessengersTypeEnum.
 *
 * Типы мессенджеров и их биты
 */
class MessengersTypeEnum
{
    const TELEGRAM = 1;
    const VIBER = 2;
    const WHATSAPP = 4;
    const ALL = 7;
}
