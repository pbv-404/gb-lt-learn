<?php
declare(strict_types=1);

namespace common\services\tasks;

/**
 * Класс помощник
 * помогает, сформировать структуру $messengers для @see MessageSubscriber.
 */
class MessengerObserverBuilder
{
    /**
     * Возвращает структуру для списка $messengers класса @see MessageSubscriber.
     *
     * @param array $constructParams
     */
    public static function create(string $class, int $messengerType, array $constructParams = null): array
    {
        return [
            'class' => $class,
            'messengerType' => $messengerType,
            'constructParams' => $constructParams ?? [],
        ];
    }
}
