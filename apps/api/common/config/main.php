<?php
return [
    'vendorPath' => dirname(__DIR__, 2).'/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];
