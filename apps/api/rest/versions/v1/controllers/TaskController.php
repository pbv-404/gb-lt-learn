<?php

namespace rest\versions\v1\controllers;

use common\models\TaskForm;
use common\services\tasks\MessageSubscriber;
use common\services\tasks\messenger\MessengerStructure;
use rest\versions\v1\results\BaseApiResult;
use yii\filters\auth\QueryParamAuth;
use yii\rest\Controller;

/**
 * Class TaskController.
 *
 * Контроллер для постановки задачи в очередь
 */
class TaskController extends Controller
{
    /**
     * @var MessageSubscriber
     */
    private $observers;

    public function init()
    {
        $this->observers = \Yii::$container->get(MessageSubscriber::class);
        $this->observers->setQueue(\Yii::$app->queue);
    }

    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::class,
        ];

        return $behaviors;
    }

    /**
     * Ставит задачу в очередь на рассылку.
     */
    public function actionCreate(): BaseApiResult
    {
        $task = new TaskForm();
        $task->load(\Yii::$app->getRequest()->getBodyParams(), '');
        if (false === $task->validate()) {
            return $this->failValidation($task);
        }

        $data = new MessengerStructure();
        $data->setNeedSend($task->dateSend)
            ->setSendTo($task->sendTo)
            ->setMessage($task->message);
        $this->observers->notify($task->messengerType, $data);

        return new BaseApiResult(true, 'Задача успешно поставлена в очередь', $task);
    }

    /**
     * Выполняет генерацию ошибочного ответа.
     */
    private function failValidation(TaskForm $form): BaseApiResult
    {
        $errors = [];
        foreach ($form->getErrors() as $field => $messages) {
            $errors[$field] = $messages;
        }

        return new BaseApiResult(false, 'При постановке задачи возникли ошибки', $errors);
    }
}
