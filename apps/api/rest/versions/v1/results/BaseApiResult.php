<?php

namespace rest\versions\v1\results;

use function get_object_vars;
use JsonSerializable;

/**
 * Class BaseApiResult.
 *
 * Базовая структура ответа АПИ
 */
class BaseApiResult implements JsonSerializable
{
    /**
     * @var bool
     */
    protected $success;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var mixed
     */
    protected $data;

    /**
     * AbstractApiResult constructor.
     *
     * @param mixed|null $data
     */
    public function __construct(bool $success, string $message = '', $data = null)
    {
        $this->success = $success;
        $this->message = $message;
        $this->data = $data;
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): self
    {
        $this->success = $success;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     *
     * @return BaseApiResult
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Преобразование в json структуру.
     */
    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
